#include "myqmlitem.h"

#include <QCursor>
#include <QSGFlatColorMaterial>

MyQMLItem::MyQMLItem(QQuickItem * parent)
	: QQuickItem(parent) {
	setFlag(ItemHasContents, true);
	setAcceptedMouseButtons(
	  Qt::LeftButton | Qt::MiddleButton | Qt::RightButton);
	setCursor({Qt::IBeamCursor});
	forceActiveFocus();
}

QSGNode * MyQMLItem::updatePaintNode(
  QSGNode *, QQuickItem::UpdatePaintNodeData *) {

	if (node == nullptr) {
		node = new QSGOpacityNode;
	}

	if (node->childCount() != coords.length()) {
		node = new QSGOpacityNode;

		for (int i = 0; i < coords.length(); i++) {
			auto * geometryNode = new QSGGeometryNode;
			auto * geometry =
			  new QSGGeometry{QSGGeometry::defaultAttributes_Point2D(), 2};

			geometry->setLineWidth(2);
			geometry->setDrawingMode(QSGGeometry::DrawLines);
			geometryNode->setGeometry(geometry);
			geometryNode->setFlag(QSGNode::OwnsGeometry);
			node->appendChildNode(geometryNode);
		}
	}
	int    index = 0;
	auto * cNode = dynamic_cast<QSGGeometryNode *>(node->firstChild());

	while (cNode != nullptr) {
		auto * cGeometry = cNode->geometry();

		QSGGeometry::Point2D * vertices = cGeometry->vertexDataAsPoint2D();

		vertices[0].set(coords[index].x(), coords[index].y());
		vertices[1].set(coords[index].x(), coords[index].y() + 20);

		cNode->setFlag(QSGNode::OwnsGeometry);
		cNode->markDirty(QSGNode::DirtyGeometry);

		QSGFlatColorMaterial * material;
		if (cNode->material() == nullptr) {
			material = new QSGFlatColorMaterial;
			cNode->setFlag(QSGNode::OwnsMaterial);
		}
		else {
			material = dynamic_cast<QSGFlatColorMaterial *>(cNode->material());
		}

		material->setColor(Qt::black);
		cNode->setMaterial(material);

		index++;
		cNode = dynamic_cast<QSGGeometryNode *>(cNode->nextSibling());
	}

	node->setOpacity(0.8);
	update();

	return node;
}

void MyQMLItem::mousePressEvent(QMouseEvent * event) {
	coords.append({event->x(), event->y()});
	qDebug() << event;
}
