#ifndef MYQMLITEM_H
#define MYQMLITEM_H

#include <QQuickItem>
#include <QSGGeometryNode>

class MyQMLItem : public QQuickItem
{
	Q_OBJECT
public:
	MyQMLItem(QQuickItem * parent = nullptr);

signals:

public slots:

	// QQuickItem interface
protected:
	QSGNode * updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
	void      mousePressEvent(QMouseEvent * event) override;

private:
	QSGOpacityNode * node = nullptr;

	QList<QPoint> coords = {{10, 10}};
};

#endif  // MYQMLITEM_H
