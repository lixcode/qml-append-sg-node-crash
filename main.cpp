#include "myqmlitem.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char * argv[]) {
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	QQmlApplicationEngine engine;

	qmlRegisterType<MyQMLItem>("MyItems", 1, 0, "MyItem");

	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
